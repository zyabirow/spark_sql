# Databricks notebook source
# import all required dependencies
from pyspark.sql.functions import col, row_number
from pyspark.sql.window import Window
from pyspark.sql.functions import expr, months_between, explode, date_format

# COMMAND ----------

# set conf for connection to storage with data
spark.conf.set("fs.azure.account.auth.type.bd201stacc.dfs.core.windows.net", "OAuth")
spark.conf.set("fs.azure.account.oauth.provider.type.bd201stacc.dfs.core.windows.net", "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider")
spark.conf.set("fs.azure.account.oauth2.client.id.<azure_storage>.dfs.core.windows.net", "<azure_storage_key>")
spark.conf.set("fs.azure.account.oauth2.client.secret.<azure_storage>.dfs.core.windows.net", "<azure_storage_key>")
spark.conf.set("fs.azure.account.oauth2.client.endpoint.<azure_storage>.dfs.core.windows.net", "https://login.microsoftonline.com/<azure_storage_key>/oauth2/token")
# COMMAND ----------


# function for creating mount point 
def sub_mount(str_path):
    if any(mount.mountPoint == str_path for mount in dbutils.fs.mounts()):
        dbutils.fs.unmount(str_path)
    else:
        dbutils.fs.mount(source="wasbs://<azure_storage>.blob.core.windows.net/test",
                         mount_point=str_path,
                         extra_configs={"fs.azure.account.key.<azure_storage>.blob.core.windows.net":
                                            "<azure_storage_key>"})


# COMMAND ----------

# add mount point, at first step unmount if neccessary 
mnt_point = "/mnt/test_mount"
sub_mount(mnt_point)

# COMMAND ----------

# initialize all main paths
expedia_bronze_path = mnt_point + "/delta/bronze/expedia"
hotel_weather_bronze_path = mnt_point + "/delta/bronze/hotelWeather"
silver_path = mnt_point + "/delta/silver/"
gold_path = mnt_point + "/delta/gold/"

# COMMAND ----------

# read expedia data in avro format
expedia_df = spark.read.format('avro').load("abfss://m07sparksql@bd201stacc.dfs.core.windows.net/expedia/")

# COMMAND ----------

# read hotel-weather data in parquet format
weather_df = spark.read.parquet("abfss://m07sparksql@bd201stacc.dfs.core.windows.net/hotel-weather/")

# COMMAND ----------

# save expedia data in bronze path in delta format
expedia_df.write.format("delta").option("overwriteSchema", "true").mode("overwrite").save(expedia_bronze_path)

# COMMAND ----------

# save hotel-weather data in bronze path in delta format
weather_df.write.format("delta").option("overwriteSchema", "true").mode("overwrite").partitionBy("year", "month", "day").save(hotel_weather_bronze_path)

# COMMAND ----------

# drop tables if exists
spark.sql("DROP TABLE IF EXISTS expedia_bronze")
spark.sql("DROP TABLE IF EXISTS hotel_weather_bronze")

# COMMAND ----------

# create tables for SQL Manipulation
spark.sql(f"CREATE TABLE expedia_bronze USING DELTA LOCATION '{expedia_bronze_path}'")
spark.sql(f"CREATE TABLE hotel_weather_bronze USING DELTA LOCATION '{hotel_weather_bronze_path}'")

# COMMAND ----------

# END "Databricks Prepare Part" 

# COMMAND ----------

# Task 1 "Top 10 hotels with max absolute temperature difference by month"

top_10_temp_diff = spark.sql('''SELECT * from (with delta_tmpr as (SELECT address, city, country, id, name, 
(MAX(avg_tmpr_c) - MIN(avg_tmpr_c)) as difference, year, month FROM hotel_weather_bronze GROUP BY address, 
city, country, id, year, month, name ORDER BY year, month, difference DESC) SELECT address, city, country, 
id, name, difference, year, month,row_number() over(partition by year, month order by difference desc) 
as rn from delta_tmpr) where rn<=10''')

# COMMAND ----------

# explain plan
top_10_temp_diff.explain()

# COMMAND ----------

# display result
display(top_10_temp_diff)

# COMMAND ----------

# Task 2 "Top 10 busy hotels for each month"

array_of_months_expr = """sequence(TRUNC(TO_DATE(srch_ci), 'MONTH'), TRUNC(TO_DATE(srch_co), 'MONTH'), interval 1 month)"""
window_in_ch_months = Window.partitionBy("checkin_months").orderBy(col("count").desc(), col("hotel_id"))
pre_df=spark.table("hotel_weather_bronze")
weather_bronze_dist_rows = pre_df.select(pre_df["id"], pre_df["name"])
topTenBusyDF = spark.table("expedia_bronze")
topTenBusyDF = topTenBusyDF.withColumn("srch_ci_correct", when(topTenBusyDF["srch_ci"] > topTenBusyDF["srch_co"],
                                                       topTenBusyDF["srch_co"]).otherwise(topTenBusyDF["srch_ci"]))
topTenBusyDF = topTenBusyDF.withColumn("srch_co_correct", when(topTenBusyDF["srch_ci"] > topTenBusyDF["srch_co"],
                                                       topTenBusyDF["srch_ci"]).otherwise(topTenBusyDF["srch_co"]))
columns_to_drop = ['srch_ci', 'srch_co']
topTenBusyDF = topTenBusyDF.drop(*columns_to_drop)
topTenBusyDF = topTenBusyDF.withColumnRenamed("srch_ci_correct","srch_ci").withColumnRenamed("srch_co_correct","srch_co")
topTenBusyDF = topTenBusyDF.withColumn("m_diff", months_between(topTenBusyDF["srch_co"], topTenBusyDF["srch_ci"]))
topTenBusyDF = topTenBusyDF.filter(topTenBusyDF["m_diff"] > 0)
topTenBusyDF = topTenBusyDF.withColumn("seq", expr(array_of_months_expr))
topTenBusyDF = topTenBusyDF.withColumn("checkin_months", explode(topTenBusyDF["seq"]))
topTenBusyDF = topTenBusyDF.groupBy(topTenBusyDF["hotel_id"], topTenBusyDF["checkin_months"]).count().alias('count')
topTenBusyDF = topTenBusyDF.select(topTenBusyDF["hotel_id"], date_format(topTenBusyDF["checkin_months"], "yyyy-MM").alias("checkin_months"), topTenBusyDF["count"], row_number().over(window_in_ch_months).alias('rnum'))
topTenBusyDF = topTenBusyDF.join(weather_bronze_dist_rows, weather_bronze_dist_rows["id"] == topTenBusyDF["hotel_id"], "left")
topTenBusyDF = topTenBusyDF.dropDuplicates(["hotel_id", "checkin_months", "count"])
topTenBusyDF = topTenBusyDF.filter(topTenBusyDF["rnum"] <= 10)
topTenBusyDF = topTenBusyDF.orderBy(topTenBusyDF["checkin_months"], topTenBusyDF["rnum"])
topTenBusyDF = topTenBusyDF.drop(topTenBusyDF["id"])

# COMMAND ----------

# explain plan and display result
topTenBusyDF.explain()
display(topTenBusyDF)

# COMMAND ----------

#  Task 3 "Calculate weather trend for visits with extended stay"

extended_stay = spark.sql('''select * from (
                          WITH delta_tmpr AS (
                          SELECT hotel_id, CAST(srch_ci AS DATE) AS in_ch, CAST(srch_co AS DATE) AS out_ch, 
                           (checkout_t.avg_tmpr_c - checkin_t.avg_tmpr_c) AS diff 
                                FROM expedia_bronze ed LEFT JOIN hotel_weather_bronze checkin_t 
                                ON ((ed.hotel_id = checkin_t.id) AND (ed.srch_ci = checkin_t.wthr_date)) 
                                LEFT JOIN hotel_weather_bronze checkout_t ON ((ed.hotel_id = checkout_t.id) 
                               AND (ed.srch_co = checkout_t.wthr_date)) 
                                WHERE (date_part('day', (to_timestamp(srch_co) - 
                               to_timestamp(srch_ci)))) > 7)
                               SELECT DISTINCT hotel_id, diff, AVG(hrd.avg_tmpr_c) 
                               OVER (PARTITION BY hrd.id) 
                               AS avg_stay_tempr FROM delta_tmpr dt 
                                LEFT JOIN hotel_weather_bronze hrd ON (dt.hotel_id = hrd.id) AND 
                               ((wthr_date >= in_ch) AND (wthr_date <= out_ch))) ORDER BY diff DESC''')



# COMMAND ----------

# explain plan and display result
extended_stay.explain()
display(extended_stay)

# COMMAND ----------

# write results to storage

top_10_temp_diff.write.format("delta").option("overwriteSchema", "true").mode("overwrite").save(silver_path)

topTenBusyDF.write.format("delta").option("overwriteSchema", "true").mode("overwrite").save(gold_path)

extended_stay.write.format("delta").option("overwriteSchema", "true").mode("overwrite").save(gold_path)
