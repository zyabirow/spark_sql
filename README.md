* Setup needed requirements into your env `pip install -r requirements.txt`
* Task 1 execution plan - total time 13 sec.
![task1-1](images/task1-1.png)
![task1-2](images/task1-2.png)
![task1-3](images/task1-3.png)
* Task 1 final table

![task1-4](images/task1-4.png)

* Task 2 execution plan - total time 17 sec.


![task2-1](images/task2-2.png)
![task2-2](images/task2-1.png)
![task2-3](images/task2-3.png)

* Task 2 final table

![task2-4](images/task2-4.png)

* Task 3 execution plan - total time 24 sec.
* Among tasks this part is the most time (resource) consuming.

![task3-1](images/task3-1.png)
![task3-2](images/task3-2.png)
![task3-3](images/task3-3.png)

