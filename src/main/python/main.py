from pyspark.sql import SparkSession
from pyspark.sql.functions import col, row_number, when
from pyspark.sql.window import Window
from pyspark.sql.functions import expr, months_between, explode, date_format


class SparkClass:

    """Class for spark"""

    def __init__(self):
        # SparkSession build
        self.spark_unit = SparkSession\
            .builder\
            .appName("SparkSql")\
            .getOrCreate()


class SparkSqlClass:

    """ Class for all data manipulation """

    def __init__(self, spark):
        # SparkSession build
        self.spark = spark

    def top_10_temp_diff(self):

        """ Task 1 (Top 10 hotels with max absolute temperature difference by month) """

        top_10 = self.spark.sql('''SELECT * from (with delta_tmpr as (SELECT address, city, country, id, name,
                                (MAX(avg_tmpr_c) - MIN(avg_tmpr_c)) as difference, year, month FROM
                                hotel_weather_bronze GROUP BY address, city, country, id, year, month, name
                                ORDER BY year, month, difference DESC) SELECT address, city, country, id, name,
                                difference, year, month,row_number() over(partition by year, month order by difference
                                desc) as rn from delta_tmpr) where rn<=10''')
        return top_10

    def top_10_busy(self):

        """ Task 2 (Top 10 busy hotels for each month) """

        array_of_months_expr = """sequence(TRUNC(TO_DATE(srch_ci), 'MONTH'), TRUNC(TO_DATE(srch_co),
        'MONTH'), interval 1 month)"""
        window_in_ch_months = Window.partitionBy("checkin_months").orderBy(col("count").desc(), col("hotel_id"))
        pre_df = self.spark.table("hotel_weather_bronze")
        weather_bronze_dist_rows = pre_df.select(pre_df["id"], pre_df["name"])
        top_ten_busy_df = self.spark.table("expedia_bronze")
        top_ten_busy_df = top_ten_busy_df.withColumn("srch_ci_correct",
                                                     when(top_ten_busy_df["srch_ci"] > top_ten_busy_df["srch_co"],
                                                          top_ten_busy_df["srch_co"])
                                                     .otherwise(top_ten_busy_df["srch_ci"]))
        top_ten_busy_df = top_ten_busy_df.withColumn("srch_co_correct", when(top_ten_busy_df["srch_ci"] >
                                                                             top_ten_busy_df["srch_co"],
                                                                             top_ten_busy_df["srch_ci"])
                                                     .otherwise(top_ten_busy_df["srch_co"]))
        columns_to_drop = ['srch_ci', 'srch_co']
        top_ten_busy_df = top_ten_busy_df.drop(*columns_to_drop)
        top_ten_busy_df = top_ten_busy_df.withColumnRenamed("srch_ci_correct", "srch_ci")\
            .withColumnRenamed("srch_co_correct", "srch_co")
        top_ten_busy_df = top_ten_busy_df.withColumn("m_diff", months_between(top_ten_busy_df["srch_co"],
                                                                              top_ten_busy_df["srch_ci"]))
        top_ten_busy_df = top_ten_busy_df.filter(top_ten_busy_df["m_diff"] > 0)
        top_ten_busy_df = top_ten_busy_df.withColumn("seq", expr(array_of_months_expr))
        top_ten_busy_df = top_ten_busy_df.withColumn("checkin_months", explode(top_ten_busy_df["seq"]))
        top_ten_busy_df = top_ten_busy_df.groupBy(top_ten_busy_df["hotel_id"], top_ten_busy_df["checkin_months"])\
            .count().alias('count')
        top_ten_busy_df = top_ten_busy_df.select(top_ten_busy_df["hotel_id"],
                                                 date_format(top_ten_busy_df["checkin_months"], "yyyy-MM")
                                                 .alias("checkin_months"), top_ten_busy_df["count"], row_number()
                                                 .over(window_in_ch_months).alias('rnum'))

        top_ten_busy_df = top_ten_busy_df.join(weather_bronze_dist_rows, weather_bronze_dist_rows["id"]
                                               == top_ten_busy_df["hotel_id"], "left")
        top_ten_busy_df = top_ten_busy_df.dropDuplicates(["hotel_id", "checkin_months", "count"])
        top_ten_busy_df = top_ten_busy_df.filter(top_ten_busy_df["rnum"] <= 10)
        top_ten_busy_df = top_ten_busy_df.orderBy(top_ten_busy_df["checkin_months"], top_ten_busy_df["rnum"])
        top_ten_busy_df = top_ten_busy_df.drop(top_ten_busy_df["id"])
        return top_ten_busy_df

    def ext_stay(self):

        """ Task 3 (Calculate weather trend for visits with extended stay ) """

        extended_stay = self.spark.sql('''SELECT * FROM(WITH delta_tmpr AS (
                                        SELECT hotel_id, CAST(srch_ci AS DATE) AS in_ch,
                                        CAST(srch_co AS DATE) AS out_ch,
                                        (checkout_t.avg_tmpr_c - checkin_t.avg_tmpr_c) AS diff
                                        FROM expedia_bronze ed LEFT JOIN hotel_weather_bronze checkin_t
                                        ON ((ed.hotel_id = checkin_t.id) AND (ed.srch_ci = checkin_t.wthr_date))
                                        LEFT JOIN hotel_weather_bronze checkout_t ON ((ed.hotel_id = checkout_t.id)
                                        AND (ed.srch_co = checkout_t.wthr_date))
                                        WHERE (date_part('day', (to_timestamp(srch_co) -
                                        to_timestamp(srch_ci)))) > 7)
                                        SELECT DISTINCT hotel_id, diff, AVG(hrd.avg_tmpr_c)
                                        OVER (PARTITION BY hrd.id)
                                        AS avg_stay_tempr FROM delta_tmpr dt
                                        LEFT JOIN hotel_weather_bronze hrd ON (dt.hotel_id = hrd.id) AND
                                        ((wthr_date >= in_ch) AND (wthr_date <= out_ch))) ORDER BY diff DESC''')
        return extended_stay
