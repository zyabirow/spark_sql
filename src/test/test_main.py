from src.main.python.main import SparkClass, SparkSqlClass

# Spark initialize
SparkObj = SparkClass()

# Init variables with path
expedia_bronze_path = "src/test/resources/expedia/"
hotel_weather_bronze_path = "src/test/resources/hotel-weather"

# Drop tables
SparkObj.spark_unit.sql("DROP TABLE IF EXISTS expedia_bronze")
SparkObj.spark_unit.sql("DROP TABLE IF EXISTS hotel_weather_bronze")

# Read data and make temporary table for SQL Query
df = SparkObj.spark_unit.read.parquet(hotel_weather_bronze_path, header=True)
df.createOrReplaceTempView("hotel_weather_bronze")
df_2 = SparkObj.spark_unit.read.parquet(expedia_bronze_path, header=True)
df_2.createOrReplaceTempView("expedia_bronze")

# Create obj for data manipulation
Data_man_object = SparkSqlClass(SparkObj.spark_unit)


def test_top_10_temp_diff():
    """ Test for task 1 (Top 10 hotels with max absolute temperature difference by month) """
    top_10_df = Data_man_object.top_10_temp_diff()
    assert (top_10_df.toPandas().iloc[0][2] == 'US' and top_10_df.toPandas().iloc[0][-1] == 1
            and top_10_df.count() == 10)


def test_top_10_busy():
    """ Test for task 2 (Top 10 busy hotels for each month) """
    top_10_busy_df = Data_man_object.top_10_busy()
    assert (top_10_busy_df.toPandas().iloc[0][0] == 2499670966273
            and top_10_busy_df.toPandas().iloc[1][2] == 1)


def test_ext_stay():
    """ Test for task 3 (Calculate weather trend for visits with extended stay ) """
    xtn_stay = Data_man_object.ext_stay()
    assert (xtn_stay.toPandas().iloc[0][0] == 1632087572481)
